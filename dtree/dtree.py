import pandas as pd

from sklearn.tree import DecisionTreeClassifier

train = pd.read_csv('./data/train/Iris_train.csv', index_col='Id')
X_train = train.drop(columns=['Species']).to_numpy()
y_train = train['Species'].to_numpy()

test = pd.read_csv('./data/test/Iris_test.csv', index_col='Id')
X_test = test.drop(columns=['Species']).to_numpy()

tree = DecisionTreeClassifier().fit(X_train, y_train)
y_pred = tree.predict(X_test).tolist()

with open('dtree/predict.txt', 'w') as f:
    for el in y_pred:
        f.write(f"{el} \n")